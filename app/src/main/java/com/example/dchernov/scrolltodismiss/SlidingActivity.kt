package com.example.dchernov.scrolltodismiss

import android.animation.Animator
import android.animation.ObjectAnimator
import android.graphics.Color
import android.graphics.Point
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.os.Build
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.MotionEvent
import android.view.View
import com.example.dchernov.scrolltodismiss.SlidingActivity.Side.*

/**
 * @author dchernov
 */
abstract class SlidingActivity : AppCompatActivity() {

    private companion object {
        const val WINDOW_MAX_ALPHA = 128
        const val GESTURE_MIN_LIMIT = 10
    }

    private enum class Side { BOTTOM, UP, RIGHT, LEFT }


    private var startX = 0f
    private var startY = 0f
    private lateinit var screenSize : Point
    private val windowDrawable: Drawable = ColorDrawable(Color.BLACK)
    private lateinit var root: View

    private var inBottomSlide = false
    private var inUpSlide = false
    private var inLeftSlide = false
    private var inRightSlide = false

    abstract fun canSlideUp(): Boolean
    abstract fun canSlideLeft(): Boolean
    abstract fun canSlideBottom(): Boolean
    abstract fun canSlideRight(): Boolean
    abstract fun getRootView(): View

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        screenSize = Point().apply { windowManager.defaultDisplay.getSize(this) }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.statusBarColor = Color.TRANSPARENT
        }

        windowDrawable.alpha = 0
        window.setBackgroundDrawable(windowDrawable)
    }

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)
        root = getRootView()
    }

    override fun dispatchTouchEvent(ev: MotionEvent): Boolean {
        var handled = false

        when (ev.action) {

            MotionEvent.ACTION_DOWN -> {
                startX = ev.x
                startY = ev.y
            }

            MotionEvent.ACTION_MOVE -> {

                when {
                    bottomSlideShouldHandle(startX, startY, ev) -> {
                        if (!inBottomSlide) {
                            inBottomSlide = true
                            ev.action = MotionEvent.ACTION_CANCEL
                            super.dispatchTouchEvent(ev)
                        }

                        val deltaY = ev.y - startY
                        root.y = if (deltaY>0) deltaY else {
                            //move slide in up side
                            if (canSlideUp()) {
                                inBottomSlide = false
                                inUpSlide = true
                            }
                            0f
                        }

                        updateWindowOnSlide(BOTTOM)
                        handled = true
                    }

                    upSlideShouldHandle(startX, startY, ev) -> {
                        if (!inUpSlide) {
                            inUpSlide = true
                            ev.action = MotionEvent.ACTION_CANCEL
                            super.dispatchTouchEvent(ev)
                        }

                        val deltaY = ev.y - startY
                        root.y = if (deltaY<0) deltaY else {
                            //move slide in bottom side
                            if (canSlideBottom()) {
                                inUpSlide = false
                                inBottomSlide = true
                            }
                            0f
                        }

                        updateWindowOnSlide(UP)
                        handled = true
                    }

                    rightSlideShouldHandle(startX, startY, ev) -> {
                        if (!inRightSlide) {
                            inRightSlide = true
                            ev.action = MotionEvent.ACTION_CANCEL
                            super.dispatchTouchEvent(ev)
                        }

                        val deltaX = ev.x - startX
                        root.x = if (deltaX>0) deltaX else {
                            //move slide in left side
                            if (canSlideLeft()) {
                                inRightSlide = false
                                inLeftSlide = true
                            }
                            0f
                        }

                        updateWindowOnSlide(RIGHT)
                        handled = true
                    }

                    leftSlideShouldHandle(startX, startY, ev) -> {
                        if (!inLeftSlide) {
                            inLeftSlide = true
                            ev.action = MotionEvent.ACTION_CANCEL
                            super.dispatchTouchEvent(ev)
                        }

                        val deltaX = ev.x - startX
                        root.x = if (deltaX<0) deltaX else {
                            //move slide in right side
                            if (canSlideRight()) {
                                inLeftSlide = false
                                inRightSlide = true
                            }
                            0f
                        }

                        updateWindowOnSlide(LEFT)
                        handled = true
                    }
                }
            }

            MotionEvent.ACTION_UP, MotionEvent.ACTION_CANCEL -> {

                when {

                    inBottomSlide -> {
                        inBottomSlide = false
                        handled = true
                        handleDismissEvent(BOTTOM, ev)
                    }

                    inUpSlide -> {
                        inUpSlide = false
                        handled = true
                        handleDismissEvent(UP, ev)
                    }

                    inRightSlide -> {
                        inRightSlide = false
                        handled = true
                        handleDismissEvent(RIGHT, ev)
                    }

                    inLeftSlide -> {
                        inLeftSlide = false
                        handled = true
                        handleDismissEvent(LEFT, ev)
                    }
                }

                //set to default position
                startX = 0f
                startY = 0f
            }

        }

        return handled || super.dispatchTouchEvent(ev)
    }

    private fun bottomSlideShouldHandle(startX: Float, startY: Float, ev: MotionEvent): Boolean {

        if (inBottomSlide) {
            return true
        }

        if (!canSlideBottom() || inUpSlide || inLeftSlide || inRightSlide) {
            return false
        }

        val deltaX = startX - ev.x
        val deltaXAbs = if (deltaX>=0) deltaX else -deltaX
        if (deltaXAbs > GESTURE_MIN_LIMIT) return false
        val deltaY = ev.y - startY
        return deltaY > GESTURE_MIN_LIMIT
    }

    private fun upSlideShouldHandle(startX: Float, startY: Float, ev: MotionEvent): Boolean {

        if (inUpSlide) {
            return true
        }

        if (!canSlideUp() || inBottomSlide || inRightSlide || inLeftSlide) {
            return false
        }

        val deltaX = startX - ev.x
        val deltaXAbs = if (deltaX>=0) deltaX else -deltaX
        if (deltaXAbs > GESTURE_MIN_LIMIT) return false
        val deltaY = startY - ev.y
        return deltaY > GESTURE_MIN_LIMIT
    }

    private fun rightSlideShouldHandle(startX: Float, startY: Float, ev: MotionEvent): Boolean {

        if (inRightSlide) {
            return true
        }

        if (!canSlideRight() || inLeftSlide || inUpSlide || inBottomSlide) {
            return false
        }

        val deltaY = startY - ev.y
        val deltaYAbs = if (deltaY>=0) deltaY else -deltaY
        if (deltaYAbs > GESTURE_MIN_LIMIT) return false
        val deltaX = ev.x - startX
        return deltaX > GESTURE_MIN_LIMIT
    }

    private fun leftSlideShouldHandle(startX: Float, startY: Float, ev: MotionEvent): Boolean {

        if (inLeftSlide) {
            return true
        }

        if (!canSlideLeft() || inRightSlide || inUpSlide || inBottomSlide) {
            return false
        }

        val deltaY = startY - ev.y
        val deltaYAbs = if (deltaY>=0) deltaY else -deltaY
        if (deltaYAbs > GESTURE_MIN_LIMIT) return false
        val deltaX = startX - ev.x
        return deltaX > GESTURE_MIN_LIMIT
    }

    private fun handleDismissEvent(side: Side, ev: MotionEvent) {
        if (shouldCloseOnDismiss(side, ev)) {
            animateForClosing(side)
        } else {
            animateToDefaultPosition(side)
        }
    }

    private fun shouldCloseOnDismiss(side: Side, motionEvent: MotionEvent): Boolean {

        val delta = when (side) {
            BOTTOM -> motionEvent.y - startY
            UP -> startY - motionEvent.y
            RIGHT -> motionEvent.x - startX
            LEFT -> startX - motionEvent.x
        }

        val sideSize = when (side) {
            //vertical
            BOTTOM, UP -> screenSize.y
            //horizontal
            RIGHT, LEFT -> screenSize.x
        }
        return delta > sideSize/3
    }

    private fun animateForClosing(side: Side) {

        val finish = when (side) {
            BOTTOM -> screenSize.y
            UP -> -screenSize.y
            RIGHT -> screenSize.x
            LEFT -> -screenSize.x
        }

        slideAnimate(side = side, finish = finish.toFloat(), shouldClose = true)
    }

    private fun animateToDefaultPosition(side: Side) {
        slideAnimate(side = side, finish = 0f, shouldClose = false)
    }

    private fun slideAnimate(side: Side, finish: Float, shouldClose: Boolean) {
        val start : Float
        val property: String

        when (side) {
            BOTTOM, UP -> {
                start = root.y
                property = "y"
            }
            RIGHT, LEFT -> {
                start = root.x
                property = "x"
            }
        }

        val positionAnimator = ObjectAnimator.ofFloat(root, property, start, finish)
        positionAnimator.duration = 100
        positionAnimator.addListener(object : Animator.AnimatorListener {

            override fun onAnimationRepeat(animation: Animator) {}

            override fun onAnimationEnd(animation: Animator) {
                updateWindowOnSlide(side)
                if (shouldClose) {
                    finish()
                }
            }

            override fun onAnimationCancel(animation: Animator) {
                updateWindowOnSlide(side)
            }

            override fun onAnimationStart(animation: Animator) {}

        })

        positionAnimator.addUpdateListener {
            updateWindowOnSlide(side)
        }

        positionAnimator.start()

    }

    private fun updateWindowOnSlide(side: Side) {

        val progress = when (side) {
            BOTTOM -> root.y / screenSize.y
            UP ->  -root.y / screenSize.y
            RIGHT -> root.x / screenSize.x
            LEFT -> -root.x / screenSize.x
        }

        val alpha = progress * WINDOW_MAX_ALPHA
        windowDrawable.alpha = (WINDOW_MAX_ALPHA - alpha).toInt()
    }

}