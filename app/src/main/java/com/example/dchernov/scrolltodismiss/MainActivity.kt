package com.example.dchernov.scrolltodismiss

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.CheckBox

/**
 * @author dchernov
 */
class MainActivity : SlidingActivity() {

    private lateinit var rootView: View
    private lateinit var slideDownCheckbox: CheckBox
    private lateinit var slideRightCheckbox: CheckBox
    private lateinit var slideUpCheckbox: CheckBox
    private lateinit var slideLeftCheckbox: CheckBox

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        rootView = findViewById(R.id.root_layout)
        slideDownCheckbox = findViewById(R.id.slide_down_checkbox)
        slideRightCheckbox = findViewById(R.id.slide_right_checkbox)
        slideUpCheckbox = findViewById(R.id.slide_up_checkbox)
        slideLeftCheckbox = findViewById(R.id.slide_left_checkbox)

        findViewById<View>(R.id.next_button).setOnClickListener {
            startActivity(Intent(this, MainActivity::class.java))
        }
    }

    override fun getRootView(): View {
        return rootView
    }

    override fun canSlideBottom(): Boolean {
        return slideDownCheckbox.isChecked
    }

    override fun canSlideRight(): Boolean {
        return slideRightCheckbox.isChecked
    }

    override fun canSlideUp(): Boolean {
        return slideUpCheckbox.isChecked
    }

    override fun canSlideLeft(): Boolean {
        return slideLeftCheckbox.isChecked
    }

}
